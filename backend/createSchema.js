const mongo = require('mongodb');
const { create } = require('./app/controllers/round.controller');
//const db = require('./app/models');

const MongoClient = mongo.MongoClient;

//const url = 'mongodb://localhost:27017';
const url = "mongodb://mongodb:27017"

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {

    if (err) throw err;

    const db = client.db("quiz_db");
    const collection = require("./app/models");
    collection.mongoose
        .connect(collection.url, {
            useNewUrlParser: true,
            useUnifiedTopology:true
        })
        .then(() => {
            console.log("Connected to database! ")
        })
        .catch(err => {
            console.log(err)
            console.log("Cannot connect to the database!")
            process.exit();
        })

    db.collection('masters').deleteAll().then((x) => {console.log("all docs deleted from masters")})
    collection.masters.create({
        "name":"q",
        "password": "a",
        "activeQuiz": ""
    }).then(master => {
        console.log("master "+ master.name + " created")
        createRounds(master)
        db.collection('quizzes').deleteMany().then((x) => {console.log("all docs deleted from quizzes")
            //create
            collection.quizzes.create(
                {  
                    "title": "Ilohhsionäres Kneipenquiz",
                    "image":"",
                    "rounds": [],
                    "teams": [],
                    "masterId": master.id,
                    "guesses": []
                }
            ).then(quiz => {
                createTeams(quiz)
            }).catch(err => console.log(err))
        })
        


        
    })
    

    createRounds = (master) => {
        db.collection('rounds').deleteMany().then((x) => {
            console.log("all docs deleted from rounds")
            collection.rounds.create({  
                "title": "Harry Potter",
                "masterId": master.id
            }).then(round => {
                round.questions.push(
                    {  
                        "roundId": round._id,
                        "content":"Welches Tier ist auf dem Wappen von Hufflepuff?",
                        "answer":"Ein  Dachs"
                    }
                )
                round.questions.push(
                    {  
                        "roundId": round._id,
                        "content":"Wie heißt die Schlange von Vodemort?",
                        "answer":"Nagini"
                    }
                )
                round.questions.push(
                    {
                        "roundId": round._id,
                        "content": "Welches Tier retten Harry und Hermine durch eine Zeitreise vor der Hinrichtung?",
                        "answer": "Hippogreif"
                    }
                )
                round.save(round).then(data => {
                    console.log("created: "+data.title)
                })

            })
            collection.rounds.create({  
                "title": "Game Of Thrones",
                "masterId": master.id
            }).then(round => {
                round.questions.push(
                    {  
                        "roundId": round._id,
                        "content": "Wie heißt der Wolf von Robb Stark?",
                        "answer": "Grauwind"
                    }
                )
                round.questions.push(
                    {  
                        "roundId": round._id,
                        "content": "Welches Haus regiert über die Weite (The Reach)?",
                        "answer": "Haus Tyrell"
                    }
                )
                round.questions.push(
                    {  
                        "roundId": round._id,
                        "content": "Nennt den Namen einer der beiden Schwestern von Aegon dem Eroberer",
                        "answer": "Rhaenys, Visenya"
                    }
                )
                round.save(round).then(data => {
                    console.log("created: "+data.title)
                })
                
            })
            

        })

        
        

    }

    createGuesses = (round) => {
        console.log("start creating Guesses")
        var answers = []
        console.log(collection.quizzes[0])
    }

    createTeams = (quiz) => {
        quiz.teams.push({  
            "name": "Quizzlibären",
            "score": 0,
            
        })
        quiz.teams.push({  
            "name": "Team Tresen",
            "score": 0,
            
        })
        quiz.teams.push({  
            "name": "Zuchtstuten",
            "score": 0,
            
        })
        quiz.save(quiz).then(
            data => {
                console.log("created: "+data.title+ " with "+ data.teams.length +" teams")
            }
        )

    }
        
    
    
    
});