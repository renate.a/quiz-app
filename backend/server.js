const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:4200"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Cross Origin middleware
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

//connect to model

const db = require("./app/models");
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology:true,
        useFindAndModify:false
    })
    .then(() => {
        console.log("Connected to database! ")
    })
    .catch(err => {
        console.log(err)
        console.log("Cannot connect to the database!")
        process.exit();
    })


// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Quizzalicious App application." });
});

//create

require("./app/routes/rounds.routes.js")(app);
require("./app/routes/quiz.routes.js")(app);
require("./app/routes/masters.routes.js")(app);
require("./app/routes/login.routes.js")(app);
require("./app/routes/guesses.routes.js")(app);
// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
