const db = require("../models");
const Round = db.rounds;

// Create and Save a new Question
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
      res.status(400).send({ message: "Round Title can not be empty!" });
      return;
    }

    // Create a Round
    const round = new Round({
      title: req.body.title,
      masterId: req.body.masterId,
      questions:[]

    });
    // Save Round in the database
    round
      .save(round)
      .then(data => {
        console.log(data)
        data.id = data._id
        data.questions = req.body.questions
        if(data.questions.length){
          data.questions.forEach( element => {
            element.roundId = data.id
            element.id = element._id
          })
        } 
        data
        .save(data)
        .then(data => {
          console.log(data)
          res.status(200).send(data) 
        })
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Round."
        });
      });
  };

// Retrieve all Round from the database.
exports.findAll = (req, res) => {
  
  Round.find()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Rounds."
      });
    });
};

//find all Rounds created by that master
exports.findAllOfMaster = (req, res) => {
  const masterId = req.params.id;
  Round.find({masterId: masterId})
    .then(data => {
      res.status(200).send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving this Masters Rounds"
      });
    });
};

// Find a single Round with an id
exports.findOne = (req, res) => {
  
  const id = req.params.id;

  Round.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Round with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Round with id " + id });
    });
  
};

// Update a Round by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Round.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot find Round with id ${id}!`
        });
      } else {
        data.id = data._id
        data.questions.forEach(element =>{
          console.log(element)
          element.id = element._id
        })
        data.save(data).then(data =>{
          res.status(200).send(data);
        })
        
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating  Round with id " + id
      });
    });
  
};

// Delete a Round with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Round.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot find Round with id ${id}!`
        });
      } else {
        res.send({
          message: "Round was deleted successfully!",
          id: id
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Round with id=" + id
      });
    });
  
};

// Delete all Questions from the database.
exports.deleteAll = (req, res) => {
  Round.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Rounds were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Rounds."
      });
    });
};

// Find Questions of this round
exports.findQuestions = (req, res) => {
  const id = req.body.id
  Round.findById(id)
    .then(data => {
      if (!data){
        res.status(404).send({ message: "Not found Round with id " + id });
      }
      else {

        res.send(data.questions);
      }
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Round with id " + id });
    });



  
};
