const db = require("../models");
const Masters = db.masters;

// Create and Save a new Question
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name) {
      res.status(400).send({ message: "Master Name can not be empty!" });
      return;
    }
    if (!req.body.password) {
      res.status(400).send({ message: "Password can not be empty!" });
      return;
    }
  
    // Create 
    const master = new Masters({
        name: req.body.name,
        password: req.body.password,
        activeQuiz: req.body.activeQuiz || ''

    });
    // Save in the database
    master
      .save(master)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the team."
          });

      });
  };

// Retrieve all Masters from the database.
exports.findAll = (req, res) => {
    Masters.find()
    .then(data => {

      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Masters."
      });
    });
};

// Find a single master with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Masters.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found: master with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving master with id " + id });
    });
  
};

// Update a Question by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }
  const id = req.params.id;

  Masters.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot find master with id: ${id}`
        });
      } else {
        res.status(200).send(req.body)
      };
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating master with id " + id
      });
    });
  
};

// Delete a Question with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Masters.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot find team with id=${id}`
        });
      } else {
        res.send({
          message: "team was deleted successfully!",
          id:id
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete team with id=" + id
      });
    });
  
};

// Delete all Masters from the database.
exports.deleteAll = (req, res) => {
  Masters.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Masters were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Masters."
      });
    });
};

//TODO: delete all that are too old


// Find all published 
exports.findAllPublished = (req, res) => {
  
};