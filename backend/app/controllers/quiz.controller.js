const db = require("../models");
const Quiz = db.quizzes;

// Create and Save a new Question
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
      res.status(400).send({ message: "Title can not be empty!" });
      return;
    }
    if (!req.body.masterId) {
      res.status(400).send({ message: "Master can not be empty!" });
      return;
    }
  
    // Create a Quiz
    const quiz = new Quiz({
        masterId: req.body.masterId,
        title: req.body.title,
        image: req.body.image || "",
        shortlink: req.body.shortlink || req.body.title.replace(/\s+/g, ''),
        rounds: [],
        teams: []

    });
  
    // Save Quiz in the database
    quiz
      .save(quiz)
      .then(data => {
        res.status(200).send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Quiz."
        });
      });
  };

// Retrieve all Quizzes from the database.
exports.findAll = (req, res) => {
  Quiz.find()
    .then(data => {
      
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Quizzes."
      });
    });
};

exports.findAllOfMaster = (req, res) => {
  const masterId = req.params.id;
  Quiz.find({masterId: masterId})
    .then(data => {
      res.status(200).send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving the quiz with Id." +masterId
      });
    });
};

// Find a single Question with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  Quiz.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found: Quiz with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Quiz with id " + id });
    });
  
};

exports.findByLink = (req, res) => {
  const link = req.params.link;
  Quiz.findOne({shortlink: link})
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found: Quiz with link " + link });
      else res.status(200).send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Quiz with link " + link });
    });
  
};

// Update a Question by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Quiz.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Quiz with id: ${id}. Maybe Quiz was not found!`
        });
      } else res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Quiz with id " + id
      });
    });
  
};

// Delete a Question with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Quiz.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Quiz with id=${id}. Maybe Quiz was not found!`
        });
      } else {
        res.send({
          message: "Quiz was deleted successfully!",
          id: id
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Quiz with id=" + id
      });
    });
  
};

// Delete all Quizzes from the database.
exports.deleteAll = (req, res) => {
  Quiz.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Quizzes were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Quizzes."
      });
    });
};

exports.createTeam = (req,res) =>{
  const id = req.params.id;
  
  Quiz.findById(id).then(data =>{
    if(!data){
      res.status(404).send({message: `Cannot find Quiz with id=${id}`})
      return
    }
    data.teams= req.body.teams 
    data.teams.forEach(element => { 
      element.id = element._id
    });
    data.save(data).then(data =>{
      res.status(200).send(data.teams[data.teams.length-1])
    })
    

  })

  
}

exports.findTeamById = (req,res) =>{
  const quizId = req.params.quizId;
  const teamId = req.params.teamId;

}

exports.updateTeam = (req,res) =>{
  const quizId = req.params.quizId;
  const teamId = req.params.teamId;
  
}




//TODO: delete all that are too old

// Find all published 
exports.findAllPublished = (req, res) => {
  
};
