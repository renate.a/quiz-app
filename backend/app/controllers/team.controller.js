const db = require("../models");
const Team = db.teams;

// Create and Save a new Question
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name) {
      res.status(400).send({ message: "Team Name can not be empty!" });
      return;
    }
  
    // Create a team
    const team = new Team({
        name: req.body.name,
        score: req.body.score,
        guesses: req.body.guesses

    });
  
    // Save team in the database
    team
      .save(team)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the team."
        });
      });
  };

// Retrieve all teams from the database.
exports.findAll = (req, res) => {
    Team.find()
    .then(data => {

      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving teams."
      });
    });
};

// Find a single team with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Team.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found: team with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving team with id " + id });
    });
  
};

// Update a Question by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Team.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update team with id: ${id}. Maybe team was not found!`
        });
      } else res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating team with id " + id
      });
    });
  
};

// Delete a Question with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Team.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete team with id=${id}. Maybe team was not found!`
        });
      } else {
        res.send({
          message: "team was deleted successfully!",
          id: id
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete team with id=" + id
      });
    });
  
};

// Delete all teams from the database.
exports.deleteAll = (req, res) => {
  Team.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} teams were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all teams."
      });
    });
};

//TODO: delete all that are too old

// Find all published 
exports.findAllPublished = (req, res) => {
  
};
