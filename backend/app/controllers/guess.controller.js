const db = require("../models");
const Guess = db.guesses;
const Quiz = db.quizzes;

// Create and Save a new Question
exports.create = (req, res) => {
    // Validate request
    if (!req.body.content) {
      res.status(400).send({ message: "Guess content can not be empty!" });
      return;
    }
    console.log(req.body)
    // Create a guess
    const guess = new Guess({
        content: req.body.content,
        teamId: req.body.teamId,
        question: req.body.question,
        isCorrect:  req.body.isCorrect

    });
    const quizId = req.params.quizId

  Quiz.findById(quizId).then(
    quiz =>{
      quiz.guesses.push(guess)
      quiz.guesses.forEach(element => {
        element.id = element._id
      })
      quiz.save(quiz).then(data =>{
        if(!data){
          res.status(404).send({message: "couldnt create empty guess"})
        }else{
          res.status(200).send(data.guesses[data.guesses.length-1])
        }
        
      })
    });

  };


// Retrieve all guessesbfrom the database.
exports.findByQuiz = (req, res) => {
  const quizId = req.params.quizId;
  let guesses = []
  
  Quiz.findById(quizId)
  .then(data => {
    if(!data){
      return
    }
    else if(!data.teams){

    }else{
      //data.teams.forEach(element => guesses.push(...element.guesses))
      res.status(200).send(data.guesses)
    }

    
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving guesses."
    });
  });
};

// Find a single Question with an id
exports.findOne = (req, res) => {
  const quizId = req.params.quizId
  const id = req.params.id;
  Quiz.findById(quizId)
  .then(data => {
    const found = data.guesses.find(element => element.id === id )
    if (!found)
      res.status(404).send({ message: "Not found: guess with id " + id });
    else res.status(200).send(found);
  })
  .catch(err => {
    res
      .status(500)
      .send({ message: "Error finding quiz " + quizId });
  });

    
  
};

// Update a Question by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }
  const quizId = req.params.quizId
  const id = req.params.id;

  Quiz.findById(quizId).then(quiz => {
    let index = quiz.guesses.findIndex(element => element.id === id)
    if(index >= 0 ){
      if(req.body.content){
        quiz.guesses.splice(index, 1, req.body)
        quiz.save(quiz).then(data => {
          res.send(data.guesses[index]);
    
        })

      }else{
        console.log("deleting guess"+id)
        quiz.guesses.splice(index, 1)
        quiz.save(quiz).then(data => {
          res.status(200).send({
            message: "guess was deleted successfully!",
            id:id
          });
    
        })
      }
      
      
    }else{
      res.status(404).send({
        message: `Cannot update guess with id=${id}!`
      });
    }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating guess with id " + id
      });
    });
  
};

// Delete a Question with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;
  const quizId = req.params.quizId
  Quiz.findById(quizId).then(quiz => {
    let index = quiz.guesses.findIndex(element => element.id === id)
    if(index >= 0 ){
      quiz.guesses.splice(index, 1)
      quiz.save(quiz).then(data => {
        res.status(200).send({
          message: "guess was deleted successfully!",
          id:id
        });
  
      })
    }else{
      res.status(404).send({
        message: `Cannot find and delete guess with id=${id}!`
      });
    }
    
  })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete guess with id=" + id
      });
    });
  
};

// Delete all guesses from the database.
exports.deleteAll = (req, res) => {
  const quizId = req.params.quizId
  Quiz.findById(quizId)
    .then(quiz=> {
      let deletedCount = quiz.guesses.length;
      quiz.guesses = []

      quiz.save(quiz).then(data =>{
        res.status.send({
          message: `${deletedCount} guesses were deleted successfully!`
        });

      })
      
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all guesses."
      });
    });
};

//TODO: delete all that are too old

// Find all published 
exports.findAllPublished = (req, res) => {
  
};
