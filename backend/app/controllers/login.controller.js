const  db = require("../models");
const Master = db.masters;

exports.findAll = (req, res) => {
  console.log(req.body)
    Master.find()
    .then(data => {

      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving masters."
      });
    });
    
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Master.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found: Master with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Master with id " + id });
    });
  
};

//loginValidaton
exports.validate = (req, res) => {
  Master.findOne(
    {"name" : req.body.name,
      "password" : req.body.password},
      {password:0}
    ).then(data => {
    if (!data)
        res.status(200).json({
          status: 'fail',
          message: 'Login failed'
        })
    else{
      res.status(200).json({
        status: 'success',
        data: data
      })
    }
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving this master."
    });
  });
    
};
