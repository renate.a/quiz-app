module.exports = app => {
    const guesses = require("../controllers/guess.controller.js");
  
    var router = require("express").Router();
  
    // Create a new guesses
    router.post("/:quizId/", guesses.create);
  
    // Retrieve all guesses of the quiz
    router.get("/:quizId/", guesses.findByQuiz);

    // Retrieve a single Question with id
    router.get("/:quizId/id/:id", guesses.findOne);

    // Retrieve a single Question with id
    //router.get("/quiz/:id", guesses.findByQuiz);
  
    // Update a Question with id
    router.put("/:quizId/id/:id", guesses.update);
  
    // Delete a Question with id
    router.delete("/:quizId/id/:id", guesses.delete);

  
    // Delete all guesses of the quiz
    router.delete("/:quizId/", guesses.deleteAll);
  
    app.use('/api/guesses', router);
  };