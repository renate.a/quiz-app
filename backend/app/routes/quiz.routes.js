module.exports = app => {
    const quizzes = require("../controllers/quiz.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Quiz
    router.post("/", quizzes.create);
  
    // Retrieve all Quizzes
    router.get("/", quizzes.findAll);

    // Retrieve a single Quiz with id
    router.get("/:id", quizzes.findOne);

    // Retrieve a single Quiz with id
    router.get("/view/:link", quizzes.findByLink);
  
  
    // Update a Quiz with id
    router.put("/:id", quizzes.update);
  
    // Delete a Quiz with id
    router.delete("/:id", quizzes.delete);

    // Create new Team
    router.put("/:id/newTeam", quizzes.createTeam);

  
    // Delete all Quizzes
    router.delete("/", quizzes.deleteAll);
  
    app.use('/api/quizzes', router);
  };