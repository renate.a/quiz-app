module.exports = app => {
    const masters = require("../controllers/master.controller.js");
    const quizzes = require("../controllers/quiz.controller.js");
    const rounds = require("../controllers/round.controller.js");
  
    var router = require("express").Router();
  
    // Create a new masters
    router.post("/", masters.create);
  
    // Retrieve all masters
    router.get("/", masters.findAll);

    // Retrieve a single master with id
    router.get("/:id", masters.findOne);
    

    // Update a Round with id
    router.put("/:id", masters.update);
    //retrieve all quizzes created by one Master
    router.get("/:id/quiz", quizzes.findAllOfMaster);
    // Retrieve all Rounds of one master
    router.get("/:id/rounds", rounds.findAllOfMaster);
    
  
    // Delete a Round with id
    router.delete("/:id", masters.delete);

  
    // Delete all Round
    router.delete("/", masters.deleteAll);
  
    app.use('/api/master', router);
  };