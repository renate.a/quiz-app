module.exports = app => {
    const teams = require("../controllers/team.controller.js");
  
    var router = require("express").Router();
  
    // Create a new teams
    router.post("/", teams.create);
  
    // Retrieve all teams
    router.get("/", teams.findAll);

    // Retrieve a single Round with id
    router.get("/:id", teams.findOne);
  
    // Update a Round with id
    router.put("/:id", teams.update);
  
    // Delete a Round with id
    router.delete("/:id", teams.delete);

  
    // Delete all Round
    router.delete("/", teams.deleteAll);
  
    app.use('/api/teams', router);
  };