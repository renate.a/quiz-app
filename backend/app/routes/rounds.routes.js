module.exports = app => {
    const rounds = require("../controllers/round.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Rounds
    router.post("/", rounds.create);
  
    // Retrieve all Rounds
    router.get("/", rounds.findAll);

    

    //router.get("/questions", rounds.findAllQuestions);

    // Retrieve a single Round with id
    router.get("/:id", rounds.findOne);
  
    // Update a Round with id
    router.put("/:id", rounds.update);
  
    // Delete a Round with id
    router.delete("/:id", rounds.delete);

  
    // Delete all Round
    router.delete("/", rounds.deleteAll);
  
    app.use('/api/rounds', router);
  };