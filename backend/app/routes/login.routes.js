module.exports = app => {
    const login = require("../controllers/login.controller");
    var router = require("express").Router();

    router.get("/" , login.findAll)
    router.get("/:id" , login.findOne)

    router.post("/" , login.validate)
    

    app.use('/api/login', router)
};