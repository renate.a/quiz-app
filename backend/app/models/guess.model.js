module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
          id: String,
          question: Object,
          content: String,
          teamId: String,
          isCorrect: Boolean
        },
        { timestamps: true }
      );
    schema.method("toJSON", function() {
      const { __v,_id, ...object } = this.toObject();
      object.id = _id;
      return object;
    })
    
    const Guess = mongoose.model("guess", schema)
    return Guess;
  };