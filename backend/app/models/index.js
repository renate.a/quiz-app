const dbConfig = require("../config/db.config.js");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.quizzes = require("./quiz.model.js")(mongoose);
db.rounds = require("./round.model.js")(mongoose);
db.guesses = require("./guess.model.js")(mongoose);
db.masters = require("./master.model.js")(mongoose);

module.exports = db;
