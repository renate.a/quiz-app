module.exports = mongoose => {
   var question = mongoose.Schema(
     {
       id: String,
       content: String,
       answer: String,
       roundId: String

     },
     { timestamps: true }
   )
    var round = mongoose.Schema(
        {
          id: String,
          masterId: String,
          title: String,
          questions: [question]
        },
        { timestamps: true }
      );

    round.method("toJSON", function() {
      const { __v,_id, ...object } = this.toObject();
      object.id = _id;
      return object;
    })
    question.method("toJSON", function() {
      const { _id, ...object } = this.toObject();
      object.id = _id;
      //console.log(schema)
      return object;
    })
    
    
    const Round = mongoose.model("round", round)
    return Round;
  };