module.exports = mongoose => {
  var team = mongoose.Schema(
    {
      id: String,
      name: String,
      score: String,
    },
    { timestamps: true }

  )
  var guess = mongoose.Schema(
    {
      id: String,
      question: Object,
      content: String,
      teamId: String,
      isCorrect: Boolean
    },
    { timestamps: true }

  )
    var quiz = mongoose.Schema(
        {
          id: String,
          shortlink: { type: String, unique: true },
          title: String,
          image: String,
          rounds: [],
          guesses: [guess],
          teams: [team],
          masterId: String
        },
        { timestamps: true }
      );
    quiz.method("toJSON", function() {
      const { __v,_id, ...object } = this.toObject();
      object.id = _id;
      return object;
    })

    team.method("toJSON", function() {
      const { __v,_id, ...object } = this.toObject();
      object.id = _id;
      return object;
    })
    
    const Quiz = mongoose.model("quiz", quiz)
    return Quiz;
  };