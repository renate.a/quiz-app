module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
            name: { type: String, required: true, unique: true },
            password: { type: String, required: true },
            quizzes: [],
            rounds: [],
            activeQuiz: String
        },
        { timestamps: true }
      );
    schema.method("toJSON", function() {
      const { __v,_id, ...object } = this.toObject();
      object.id = _id;
      return object;
    })
    
    const Master = mongoose.model("master", schema)
    return Master;
  };