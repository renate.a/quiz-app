module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
          id: String,
          name: String,
          score: Number,
        },
        { timestamps: true }
      );
    schema.method("toJSON", function() {
      const { __v,_id, ...object } = this.toObject();
      object.id = _id;
      return object;
    })
    
    const Team = mongoose.model("team", schema)
    return Team;
  };
