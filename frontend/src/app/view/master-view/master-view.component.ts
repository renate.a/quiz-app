import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GuessService } from 'app/guess.service';
import { MasterService } from 'app/master.service';
import { App } from 'app/model.interface';
import { QuizService } from 'app/quiz.service';
import { RoundService } from 'app/round.service';

@Component({
  selector: 'app-master-view',
  templateUrl: './master-view.component.html',
  styleUrls: ['./master-view.component.css']
})
export class MasterViewComponent implements OnInit {
  private master: App.Master
  private quizzes: App.Quiz[]
  private currentQuiz: App.Quiz
  private isReady: number = 0
  constructor(private router: Router, private roundService: RoundService, private masterService: MasterService, private quizService: QuizService, private guessService: GuessService) { }

  ngOnInit(): void {
    const masterId = localStorage.getItem('masterId');
    this.masterService.getById(masterId).subscribe(data => {
      this.master = data
      console.log('master is '+ this.master.name)
      this.roundService.getByMaster(this.master).subscribe(
        rounds =>{
            console.log("master has rounds: "+ rounds.length);
            this.isReady ++
        });
      this.quizService.getByMaster(this.master).subscribe(data => {
        console.log('quizzes are '+ data.length)
        this.quizzes = data
        this.currentQuiz = data.find(element => element.id === this.master.activeQuiz)
        if(this.currentQuiz){
          console.log('current is '+ this.currentQuiz.title)
          this.quizService.setCurrent(this.currentQuiz)
          this.guessService.getByQuiz(this.currentQuiz).subscribe(data => {
            //this.router.navigate[`${this.master.name}/quizzes`]
            this.isReady++
          })
        }else{
          console.log('No Quiz currently running')
          //this.router.navigate[`${this.master.name}/quizzes`]
          this.isReady++
          
        }
      })
      

    },(err)=>{
      if(!this.master) this.router.navigate['/error']
    },
    () => {
      console.log("complete")
      if(!this.master) this.router.navigate['./../login']
    })

  }

}
