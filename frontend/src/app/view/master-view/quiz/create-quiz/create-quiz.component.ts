import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { QuizService } from 'app/quiz.service';
import { MasterService } from 'app/master.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JQueryStyleEventEmitter } from 'rxjs/internal/observable/fromEvent';
import { QuizOverviewComponent } from '../quiz-overview/quiz-overview.component';

@Component({
  selector: 'app-create-quiz',
  templateUrl: './create-quiz.component.html',
  styleUrls: ['./create-quiz.component.css']
})
export class CreateQuizComponent implements OnInit {
  form = this.fb.group({
    quiztitle: ['', [Validators.required, Validators.min(1)] ],
    
  })

  private isSaved: boolean = false
  constructor(private fb: FormBuilder, private quizService: QuizService, private masterService: MasterService, private parent: QuizOverviewComponent) { }

  ngOnInit(): void {

  }
  createQuiz(){
    const title = this.form.get('quiztitle').value
    const master = this.masterService.getCurrentMaster()
    this.quizService.create(title, master.id).subscribe(
      data => {
        this.isSaved = true
        this.parent.ngOnInit()
      })
  }

}
