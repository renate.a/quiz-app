import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckRoundComponent } from './check-round.component';

describe('CheckRoundComponent', () => {
  let component: CheckRoundComponent;
  let fixture: ComponentFixture<CheckRoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckRoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckRoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
