import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { App } from 'app/model.interface';
import { QuizService } from 'app/quiz.service';
import { GuessService } from 'app/guess.service';
import { elementAt } from 'rxjs/operators';


@Component({
  selector: 'app-check-round',
  templateUrl: './check-round.component.html',
  styleUrls: ['./check-round.component.css']
})
export class CheckRoundComponent implements OnInit {
  private quiz: App.Quiz;
  private closedRounds: App.QuizRound[];
  private allGuesses: App.Guess[] = [];
  form

  constructor(private quizService: QuizService, private guessService: GuessService) { }

  ngOnInit(): void {
    
    this.quiz = this.quizService.getCurrent()
    this.closedRounds = this.quiz.rounds.filter(element => element.isClosed == true)
    this.allGuesses = this.quiz.guesses

    this.form = new FormGroup({});
    this.allGuesses.forEach(element => {
      const keyName = `checkbox-${element.id}`;
      this.form.addControl(keyName, new FormControl(element.isCorrect));
    });
  }

  getGuessesByRound(round: App.Round): App.Guess[]{
    var guessesInRound: App.Guess[] = []
    round.questions.forEach(question =>{
      guessesInRound = this.allGuesses.filter(element => element.question.roundId === round.id)
      //guessesInRound.push(...this.guessService.getByQuestion(question))
    })
    return guessesInRound
  }
  guessesOfQuestion(question: App.Question):App.Guess[]{
    let guesses = this.allGuesses.filter(element => element.question.id === question.id)
    return guesses

  }

  findTeam(guess: App.Guess): App.Team{
    let team = this.quiz.teams.find(element => element.id === guess.teamId)
    return team
  }

  toggleCorrect($event, guess: App.Guess){
    let team = this.quiz.teams.find(element => element.id == guess.teamId)
    
    if($event.target.checked){
      guess.isCorrect = true;
      team.score ++ 
    } else {
      guess.isCorrect = false;
      team.score --; 
    }
    this.guessService.update(guess, this.quiz).subscribe(
      data =>{
        this.quizService.updateTeam(team, this.quiz).subscribe(
          data => {
            let index = this.quiz.teams.findIndex(element => element.id === data.id)
            this.quiz.teams = data.teams
          }
        )
      }
    )


  }
}
