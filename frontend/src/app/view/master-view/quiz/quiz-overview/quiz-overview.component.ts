import { Component, OnInit } from '@angular/core';
import { App } from 'app/model.interface';
import { QuizService } from 'app/quiz.service';
import { MasterService } from 'app/master.service';

@Component({
  selector: 'app-quiz-overview',
  templateUrl: './quiz-overview.component.html',
  styleUrls: ['./quiz-overview.component.css']
})
export class QuizOverviewComponent implements OnInit {
  private master: App.Master;
  private quizzes: App.Quiz[]
  private currentQuiz: App.Quiz
  private isReady: number = 0

  constructor(private quizService: QuizService, private masterService: MasterService) { }

  ngOnInit(): void {
    
    this.master = this.masterService.getCurrentMaster()
    this.quizService.getByMaster(this.master).subscribe(data =>{
      this.quizzes = data
      this.currentQuiz = this.quizzes.find(element => element.id === this.master.activeQuiz) || {id:'', masterId: '', title:'', rounds: []}
      this.isReady++
    })
    
    
  }

  toggleCurrent(quiz: App.Quiz){
    if(quiz.id === this.currentQuiz.id){
      //no quiz is active
      this.currentQuiz = {id: '',title:'', rounds:[],masterId:''}

      this.master.activeQuiz = this.currentQuiz.id
      

    }else{
      this.master.activeQuiz = quiz.id
      this.currentQuiz = quiz
    }
    this.masterService.update(this.master).subscribe(data =>{
      this.master = data
      this.quizService.setCurrent(this.currentQuiz)
    })
    
  }

  deleteQuiz(quiz: App.Quiz){
    this.quizService.delete(quiz.id).subscribe(
      data => {
        const index = this.quizzes.findIndex(element => element.id == data.id)
        if(index >=0 ) {this.quizzes.splice(index, 1)}
        if(this.master.activeQuiz === data.id){ 
          this.master.activeQuiz = ''
          this.masterService.update(this.master).subscribe(data =>{
            this.master = data
            
          })
        }
      }
    )
    
  }

}
