import { Component, ElementRef, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GuessService } from 'app/guess.service';
import { MasterService } from 'app/master.service';
import { App } from 'app/model.interface';
import { QuestionService } from 'app/question.service';
import { QuizService } from 'app/quiz.service';
import { RoundService } from 'app/round.service';

@Component({
  selector: 'app-view-quiz',
  templateUrl: './view-quiz.component.html',
  styleUrls: ['./view-quiz.component.css']
})
export class ViewQuizComponent implements OnInit {
  private quiz: App.Quiz = {title: "",masterId: '', rounds: []};
  private link: string
  private questions: App.Question[] = [];
  private guesses: App.Guess[]
  
  private allRounds: App.Round[] =[];
  private availableRounds: App.Round[] = []
  private runningRound: App.QuizRound
  private master: App.Master;
  private isReady: number = 0;

  private activeQuestions: App.Question[] = [];
  
  private toggleType: string = "checkbox";
  remainingRounds: any;
  private emptyRound: App.QuizRound = { round: {id:'', title: '', questions: [], masterId: ''}, activatedQuestions: [], isRunning: false, isClosed: false }

  constructor(private fb: FormBuilder,private roundService: RoundService, private masterService: MasterService, private quizService: QuizService, private guessService: GuessService, private router: Router, private route: ActivatedRoute) { 

  }
  

  form: FormGroup

  ngOnInit(): void {
    this.master = this.masterService.getCurrentMaster()
    this.form = new FormGroup({})
  
    if(this.master.activeQuiz.length){
      this.quizService.getById(this.master.activeQuiz).subscribe(data => {
        this.quiz = data
        let shortlink = this.quiz.shortlink || ''
        this.form.addControl('shortlink', new FormControl(shortlink))
        this.runningRound = data.rounds.find(element => element.isRunning= true)
        if(this.runningRound){
          this.activeQuestions = this.runningRound.activatedQuestions || []
        }else{
          this.runningRound = this.emptyRound
        }
        this.isReady++
    
        
      },(err) => {
        console.log("Error retrieving active Quiz")
        this.router.navigate(['../quizzes'],{relativeTo: this.route})
      })

    }else{
      console.log("No Quiz currently active, redirecting to Quizzes Overwiew")
      this.router.navigate(['../quizzes'],{relativeTo: this.route})
    }
   
    
      
    
    this.link = "home"+this.quiz.id
    this.runningRound = this.quiz.rounds.find(element => element.isRunning) || this.emptyRound
    

   
    this.roundService.getByMaster(this.master).subscribe(
      data =>{
        this.allRounds = data
        this.generateButtons(this.quiz.rounds)
        this.generateRoundList()
        this.isReady ++
      }
    )
  }

  generateButtons(array: any[]){
    array.forEach(element => this.questions.push(...element.round.questions))
    this.questions.forEach(allQuestion => {
      this.addControlToForm(allQuestion)
      
    });

  }
  addControlToForm(question: App.Question){
    const keyName = `toggle-${question.id}`;
      console.log("create toggle for id:" +keyName)
      const active = this.activeQuestions.find(activeQuestion => activeQuestion.id === question.id)
      if(active){
        this.form.addControl(keyName, new FormControl(true));
      }else{
        this.form.addControl(keyName, new FormControl(false));
      }

  }

  generateRoundList(){
    this.availableRounds = []
    this.allRounds.forEach((allround) => {
      if(! this.quiz.rounds.find(element => element.round.id === allround.id)) this.availableRounds.push(allround)

    })
    
  }
  generateLink(linkId: string){
    this.link = "home/"+linkId
  }
  
  toggleActive(question: App.Question){
    const controlId = `toggle-${question.id}`
    
    //close Round if Question is in a new Round
    if(this.runningRound.round.id !== question.roundId){
      this.toggleRound(this.runningRound)
    }

    //check if this question is already active  
    if(this.isActive(question)){
      

      this.runningRound.activatedQuestions.splice(this.runningRound.activatedQuestions.findIndex(element => question.id === element.id), 1)
      let index = this.quiz.rounds.findIndex(element => element.round.id === this.runningRound.round.id)
      this.quiz.rounds[index] = this.runningRound
      this.quizService.update(this.quiz).subscribe(
        data => {
          
        },
        error => {console.log("error updating active questions")},
        () => {
          this.form.get(controlId).setValue("false");
          //console.log("removed from active questions")
        }
      )
      //deactivate Round if no questions are active
      if(!this.runningRound.activatedQuestions.length){
        this.quiz.rounds.forEach(element => element.isRunning = false)
        this.runningRound = this.emptyRound
      }
      
    //add to active questions
    }else{
      this.runningRound = this.quiz.rounds.find(element => element.round.id === question.roundId)
      this.runningRound.isRunning = true
      this.runningRound.activatedQuestions.push(question);

      let index = this.quiz.rounds.findIndex(element => element.round.id === question.roundId)
      this.quiz.rounds[index] = this.runningRound
      this.quizService.update(this.quiz).subscribe(
        data => {
            
        },
        error => {console.log("error updating active questions")},
        () => {
          this.form.get(controlId).setValue("true");
          //console.log("added to active questions")
        }
      )
      
    }
  }

  isActive(question: App.Question):boolean {
    let foundQuestion = this.runningRound.activatedQuestions.find(element => question.id === element.id)
    if(foundQuestion){
      return true
    }else{
      return false
    }
  }

  findQuestionNr(qRound: App.QuizRound, question: App.Question): any{
    let index = qRound.activatedQuestions.findIndex(element => question.id === element.id)
    if(index >= 0){
      return index+1
    }else{
      return ""
    }

  }

  toggleRound(round: App.QuizRound){
    
    if(round.isClosed) {
      //reopen round
      round.isClosed = false
      
      this.quizService.update(this.quiz).subscribe(
        data => {
          //open round and mark previously active questions
          if( round.activatedQuestions.length ){
            round.isRunning = true
            this.runningRound = round
            let otherRounds = data.rounds.filter( element => element.round.id !== round.round.id)
            if(otherRounds){
              otherRounds.forEach(element => {
                if(!element.isClosed){
                  element.isRunning = false
                  element.activatedQuestions = []
                }
              })

            }
            

          }
          
          //console.log("successfully updated closed Rounds")
        },
        error => {console.log("error updating closed Rounds")}
      )

    }else {
      //close round
      round.isClosed = true
      round.isRunning = false
      
      if(this.runningRound.round.id === round.round.id){
        this.runningRound = this.emptyRound
      }
      
    }

    let index = this.quiz.rounds.findIndex(element => element.round.id === round.round.id)
    
    this.quiz.rounds[index] = round
    this.quizService.update(this.quiz).subscribe(
        data => {
         // console.log("successfully updated closed Rounds")
        },
        error => {console.log("error updating closed Rounds")}
    )
  }

  toggleCorrect(guess: App.Guess){
    guess.isCorrect = !guess.isCorrect;
    this.guessService.update(guess, this.quiz).subscribe()


  }

  removeRoundFromQuiz(round: App.QuizRound){
    if(this.runningRound.round.id === round.round.id){
      //set to nempty
      this.runningRound = this.emptyRound
    }

    const index  = this.quiz.rounds.findIndex(el => el.round.id === round.round.id);
    if(index < 0){
      console.log("Could not Delete because it was not there")
    } else {
      this.quiz.rounds.splice(index, 1)
      //delete form controls for this round
      round.round.questions.forEach(element =>{
        const keyName = 'toggle-' +element.id
        this.form.removeControl(keyName)
      })

      //delete all of this rounds guesses
      let guessesOfRound = this.quiz.guesses.filter(guess => guess.question.roundId === round.round.id) 
      if(guessesOfRound.length){
        guessesOfRound.forEach(element => {
          this.guessService.deleteById(element.id, this.quiz).subscribe(data => {
            console.log(data)
          })
        })

      }
      
      
      this.updateQuiz()
    }
    
  }

  addRoundToQuiz(round: App.Round){
    const index  = this.quiz.rounds.findIndex(el => el.round.id === round.id);
    if(index > 0){
      console.log("Could not add because its already there")
    } else {
      this.quiz.rounds.push({round: round, activatedQuestions: [], isRunning: false, isClosed: false})
      round.questions.forEach(element => {
        this.addControlToForm(element)
      });
      this.updateQuiz()
    }
  }

  private updateQuiz(){
    this.quizService.update(this.quiz).subscribe(
      data => {
        //console.log(data)
        //this.quiz = data
        //this.generateButtons(data.rounds)
        this.generateRoundList()
        //this.generateButtons(this.quiz.rounds)
      }, error => {},
      () => {
        
        
      }
    )

  }
  
  guessesOfQuestion(question: App.Question):App.Guess[]{
    //console.log(this.quiz.guesses)
    let guessesOfQuestion = this.quiz.guesses.filter(element => element.question.id === question.id)
    return guessesOfQuestion
  }

  updateShortlink(shortlink: string){
    this.quiz.shortlink = shortlink
    this.quizService.update(this.quiz).subscribe(
      data => {
        this.router.navigate(['../../quiz/' , shortlink])
        //this.quiz = data
        //this.generateButtons(data.rounds)
      }, error => {
        
      },
      () => {
        this.generateRoundList()
        //this.generateButtons(this.quiz.rounds)
        
      }
    )
  }

  private resetQuiz(quiz: App.Quiz, options: string[]){
      this.quizService.reset(quiz, options).subscribe()
  }

  private resetQuizRound(round: App.QuizRound){
    round.activatedQuestions = []
    round.isRunning = false
    round.isClosed = false
  }


}
