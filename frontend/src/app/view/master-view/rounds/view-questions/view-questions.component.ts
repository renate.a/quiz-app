import { Component, OnInit } from '@angular/core';
import { App } from 'app/model.interface';
import { QuestionService } from 'app/question.service';
import { QuizService } from 'app/quiz.service';
import { RoundService } from 'app/round.service';
import { MasterService } from 'app/master.service';
import { element } from 'protractor';

@Component({
  selector: 'app-view-questions',
  templateUrl: './view-questions.component.html',
  styleUrls: ['./view-questions.component.css']
})
export class ViewQuestionsComponent implements OnInit {
  private master: App.Master;
  private rounds: App.Round[] = [];
  private questions: App.Question[] = []
  private isReady: number = 0;
  private quiz: App.Quiz = {title:"",masterId: '', rounds: []}
  constructor(private quizService: QuizService, private roundService: RoundService, private userService: MasterService) { }

  ngOnInit(): void {
    this.master = this.userService.getCurrentMaster()
    /*
    this.roundService.getByMaster(this.master).subscribe(
      data => {
        this.rounds = data
        this.isReady ++
        //this.loadQuestions();
        
      }
    )
    */
    //this.quiz = this.quizService.getCurrent()
    
  }


  deleteQuestion(round: App.Round, question: App.Question){
    this.roundService.update(round).subscribe(
        data => {round = data}
    )
    
  }

  deleteRound(round:App.Round){
    this.roundService.delete(round.id).subscribe(
      data => {}
    )
  }


}
