import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { App } from 'app/model.interface';
import { QuestionService } from 'app/question.service';
import { QuizService } from 'app/quiz.service';
import { RoundService } from 'app/round.service';
import { MasterService } from 'app/master.service';

@Component({
  selector: 'app-question-edit',
  templateUrl: './question-edit.component.html',
  styleUrls: ['./question-edit.component.css']
})
export class QuestionEditComponent implements OnInit {
  isReady: boolean = true;
  private master: App.Master
  @Input() type: string;
  @Input() round: App.Round = {id: null, title: '', questions: [], masterId: ''};
  questionsOfThisRound: App.Question[] = [];

  constructor(private roundService: RoundService ,private userService: MasterService, private fb: FormBuilder) { }
  form = this.fb.group({
    roundtitle: [this.round.title, [Validators.required, Validators.min(1)] ],
    newquestion: ['', [Validators.required, Validators.min(1)]],
    newanswer: ['', [Validators.required, Validators.min(1)]],
    questionNumber: ['']
  })
  

  ngOnInit(): void {
    if(this.type === "create"){
      this.round = {id: null, title: '', questions: [], masterId: ''}
    }else{
      this.questionsOfThisRound = this.round.questions
      this.form.get('roundtitle').setValue(this.round.title);
      this.form.get('questionNumber').setValue(this.round.questions.length + 1)
    }
    this.master = this.userService.getCurrentMaster()
  }
  
  saveQuestion(question : App.Question){
    let newQuestion: App.Question = {
      roundId: this.round.id,
      content: this.form.get('newquestion').value, 
      answer: this.form.get('newanswer').value,
    }
    this.questionsOfThisRound.push(newQuestion)

    this.form.get('newquestion').setValue('');
    this.form.get('newanswer').setValue('');
    this.form.get('questionNumber').setValue(this.questionsOfThisRound.length +1 );
  }

  deleteQuestion(question: App.Question){
    this.questionsOfThisRound.splice( this.questionsOfThisRound.findIndex(el => el === question), 1)
  }

  saveName(title: string){
    this.round.title = title
  }

  saveRound(){
    
    let newData: App.Round = {
      title: this.form.get('roundtitle').value,
      masterId: this.master.id,
      questions: this.questionsOfThisRound
    }
    if(this.round.id){
      //update
      console.log("update")
      console.log(newData)
      newData.id = this.round.id
      this.roundService.update(newData).subscribe(
        data => {
          console.log(`round ${data.title} updated`)
        }
      )
    }else{
      //create
      console.log("create")
      console.log(newData)
      this.roundService.create(newData).subscribe(
        data => {
          console.log(`round ${data.title} created`)
        })
    }

    this.round.questions = this.questionsOfThisRound
    
  }

  deleteRound(){
    this.roundService.delete(this.round.id).subscribe(
      data => {
        this.round = {id: null, title: '', questions: [], masterId: ''}
        this.questionsOfThisRound = []
        console.log(`round ${data.id} deleted`)
      }
    )
  }
}
