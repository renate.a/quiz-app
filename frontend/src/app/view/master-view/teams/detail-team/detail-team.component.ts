import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { App } from 'app/model.interface';
import { QuizService } from 'app/quiz.service';
import { GuessService } from 'app/guess.service';
import { TeamOverviewComponent } from '../team-overview/team-overview.component';
import { TeamNavigationComponent } from 'app/view/master-view/teams/team-navigation/team-navigation.component';

@Component({
  selector: 'app-detail-team',
  templateUrl: './detail-team.component.html',
  styleUrls: ['./detail-team.component.css']
})
export class DetailTeamComponent implements OnInit {
  private quiz : App.Quiz
  private rounds: App.QuizRound[];
  private closedRounds: App.QuizRound[]
  @Input() inputTeam: App.Team
  private team: App.Team
  private isReady: boolean
  
  form = new FormGroup({
    
  })
  constructor(private quizService: QuizService, private guessService: GuessService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.isReady = false
    this.quiz = this.quizService.getCurrent()
    this.route.paramMap.subscribe(params => {
      const id = params['params']['id'];
      
      if(id){
        this.team = this.quiz.teams.find(element => element.id === id)
        this.team.guesses = this.quiz.guesses.filter(element => element.teamId === this.team.id)
      }else if(this.inputTeam.id){
        this.team = this.inputTeam
      }else{
        console.log("this team isnt part of the quiz");
      }
      
      this.isReady = true
    });

    
    this.rounds = this.quiz.rounds;
    this.closedRounds = this.quiz.rounds.filter(element => element.isClosed === true);
  }
  toggleCorrect($event, guess: App.Guess){
    
    if($event.target.checked){
      guess.isCorrect = true;
      this.team.score++;  
    } else {
      guess.isCorrect = false;
      this.team.score--; 
    }

    this.quizService.updateTeam(this.team, this.quiz).subscribe()
    //this.guessService.update(guess, this.quiz).subscribe()
  }

  guessesInRound(round: App.Round): App.Guess[]{
    const guessesInRound = this.team.guesses.filter(guess =>{ if(guess.question.roundId === round.id){ return guess }});

    return guessesInRound
  }

}
