import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { App } from 'app/model.interface';
import { QuizService } from 'app/quiz.service';
import { TeamNavigationComponent } from 'app/view/master-view/teams/team-navigation/team-navigation.component';

@Component({
  selector: 'app-team-overview',
  templateUrl: './team-overview.component.html',
  styleUrls: ['./team-overview.component.css']
})
export class TeamOverviewComponent implements OnInit {
  quiz: App.Quiz
  teams: App.Team[]
  constructor(private quizService: QuizService, private parent: TeamNavigationComponent, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.quiz = this.quizService.getCurrent() || null
    if(!this.quiz) {
      this.router.navigate(['../quizzes'], {relativeTo: this.route})
    }
    this.teams = this.quiz.teams
  }

}
