import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { App } from 'app/model.interface';
import { QuizService } from 'app/quiz.service';
import { DetailTeamComponent } from 'app/view/master-view/teams/detail-team/detail-team.component';

@Component({
  selector: 'app-team-navigation',
  templateUrl: './team-navigation.component.html',
  styleUrls: ['./team-navigation.component.css']
})
export class TeamNavigationComponent implements OnInit {
  private teamId: string
  private isReady: number= 0
  private quiz: App.Quiz
  private teams: App.Team[]
  private team: App.Team

  constructor(private route: ActivatedRoute, private router: Router, private quizService: QuizService) { }
  

  ngOnInit(): void {
    this.teamId = this.findId(this.router.url)
    
    this.quiz = this.quizService.getCurrent() || {id: '',title:'', masterId:'', rounds:[]}
  
    if(this.quiz.id.length) {
      this.teams = this.quiz.teams || []
      this.isReady ++
    }else{
      this.teams = []
      console.log("No Quiz currently active, redirecting to Quiz Overview")
      this.router.navigate(['/../master/quizzes'], {relativeTo: this.route})
    }
    
  }
  findId(url:string): string{
    url = url.slice(7, url.length)
    return url
  }

  setTeam(team: App.Team){
    this.teamId = team.id
    this.team = team
  }
}
