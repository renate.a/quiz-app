import { Route } from '@angular/compiler/src/core';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { App } from 'app/model.interface';
import { QuizService } from 'app/quiz.service';
//import { TeamService } from 'app/team.service';
import { ViewGuessesComponent } from 'app/view/user-view/view-guesses/view-guesses.component';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  localStorage: Storage;
  isReady: number = 0
  @Input() quiz : App.Quiz = {id:'',title:'',masterId: '', rounds:[]}

  constructor(private fb: FormBuilder, private quizService : QuizService, private router: Router, private route: ActivatedRoute) {
    this.localStorage = window.localStorage;
   }
  form = this.fb.group({
    teamname: ['',[Validators.required, Validators.min]]
  })

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const linkId = params['params']['linkId'];
      this.quizService.getByLink(linkId).subscribe(
        data => {
          this.quiz = data
          console.log(this.quiz)
          localStorage.removeItem('teamId')
          this.isReady ++
          
        },(error) => {
          // Redirect to "not Found page"
          console.log("Quiz with this linkId not found")
        })
    })
  }

  setTeam(teamname: string){
    
        this.quizService.createTeam(teamname, this.quiz).subscribe(data => { 
          //this.quiz.teams.push(newTeam)
          localStorage.setItem('teamId', data.id)
          this.router.navigate(['../'], {relativeTo: this.route});
        })
  }

}
