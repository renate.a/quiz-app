import { Component, ElementRef, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GuessService } from 'app/guess.service';
import { App } from 'app/model.interface';
import { QuestionService } from 'app/question.service';
import { QuizService } from 'app/quiz.service';
import { RoundService } from 'app/round.service';
import { SignInComponent } from 'app/view/user-view/sign-in/sign-in.component';


@Component({
  selector: 'app-view-guesses',
  templateUrl: './view-guesses.component.html',
  styleUrls: ['./view-guesses.component.css']
})
export class ViewGuessesComponent implements OnInit {
  private masterView: boolean
  private team: App.Team = {name:'', score: 0};
  private teamId: string = ''
  private round: App.Round = {id:'', title:'', questions: [], masterId: ''};
  private quiz: App.Quiz = {id:'',title: '', masterId: '', rounds:[]};
  private activeQuestions: App.Question[];
  private newestQuestion: App.Question;
  private isReady: number = 0;
  private isRegistered: boolean;

  localStorage: Storage;
  form = new FormGroup({}) 
  

  constructor(private route: ActivatedRoute, private quizService: QuizService, private guessService: GuessService, private router: Router) { 
    this.localStorage = window.localStorage
  }

  ngOnInit(): void {
    this.teamId = localStorage.getItem('teamId')
    this.route.paramMap.subscribe(params => {
      const linkId = params['params']['linkId'];
      this.quizService.getByLink(linkId).subscribe(
        data => {
          this.quiz = data

          if(data.masterId === localStorage.getItem('masterId')){
            this.masterView = true
          }

          if(this.quiz.teams.find(element => element.id === this.teamId)){
            this.isRegistered = true
          }else{
            this.router.navigate(['sign-in'],{relativeTo: this.route})
            this.isRegistered = false
          }
          
          
        },(error) => {
          // Redirect to "not Found page"
          console.log("Quiz with this linkId not found")
        }, () => {
          if(this.quiz.rounds.find(element => element.isRunning == true)){
            this.round = this.quiz.rounds.find(element => element.isRunning == true).round
            this.activeQuestions = this.quiz.rounds.find(element => element.isRunning === true).activatedQuestions
            this.newestQuestion = this.activeQuestions[this.activeQuestions.length-1]
          }else{
            console.log("keine Frage ist Aktiv")
            this.round = {id:"", title:"Keine Frage ist Aktiv", questions: [], masterId: ''}
            this.activeQuestions = []
          }

          if(this.isRegistered){
            this.loadTeam(this.teamId)
          }
          

          
        }
      )
      
    });

    
  }
  isSaved(question:App.Question): boolean{
    let content = this.form.get('guess-'+ question.id).value
    let existingGuess = this.team.guesses.find(element => element.question.id === question.id)
    if(existingGuess && existingGuess.content === content){
      return true
    }else if(existingGuess && existingGuess.content !== content){
      return false
    }else{
      return false
    }
    

  }

  private loadTeam(teamId: string){
    this.isReady = 0
    this.team = this.quiz.teams.find(element => element.id === teamId)
    if(!this.team){
      this.team = null
      this.teamId = ''
      localStorage.removeItem('teamId')

    }else{
      localStorage.setItem('teamId', teamId)
        this.teamId = teamId
        if(this.quiz.guesses) this.team.guesses = this.quiz.guesses.filter(element => element.teamId === this.team.id)
        this.form = new FormGroup({}) 
        this.activeQuestions.forEach(element => {
          const keyName = `guess-${element.id}`;
          this.form.addControl(keyName, new FormControl(''));
          
          const existingGuess = this.team.guesses.find(guess => guess.question.id === element.id )
          if(existingGuess){
            
            this.form.get('guess-' + element.id).setValue(existingGuess.content)
          }
          
        });
      this.isReady ++
    }


  }

  redirectMaster(){
    this.router.navigate(['../../master/active'], {relativeTo: this.route})
  }

  submitForm(){
    this.activeQuestions.forEach(question => {
      const formControlName = 'guess-' + question.id;
      let guess: string = this.form.get(formControlName).value || ''
      let existingGuess = this.team.guesses.find(element => element.question.id === question.id) || {content:'',teamId: this.teamId, question: question}
      if(guess.length && existingGuess.content.length && existingGuess.content !== guess){
        //update guess
        console.log("update guess to: "+ guess)
        
        existingGuess.content = guess
        //this.quiz.guesses.splice(index, 1, existingGuess)
        //this.quizService.updateTeam(this.team, this.quiz).subscribe()
        this.guessService.update(existingGuess, this.quiz).subscribe(data => {
          let index = this.team.guesses.findIndex(element => data.id === element.id)
          if(index>= 0) this.team.guesses.splice(index, 1, data)
        })
        
        
      }else if(guess.length && !existingGuess.content.length){
        //create new guess
        
        let newGuess: App.Guess = {
          content: this.form.get(formControlName).value, 
          question: question, 
          teamId: this.team.id
        }
        this.guessService.create(newGuess.content, question, this.team, this.quiz).subscribe(
          data => {
            this.team.guesses.push(data)
         
            //this.quizService.updateTeam(this.team, this.quiz).subscribe()
        })
      }else if(!guess.length && existingGuess.content.length){
        //delete guess
        
        this.guessService.deleteById(existingGuess.id, this.quiz).subscribe(
          data => {
            let index = this.quiz.guesses.findIndex(element => existingGuess.id == element.id)
            this.team.guesses.splice(index, 1)
            //this.quizService.updateTeam(this.team, this.quiz).subscribe()
            
          }
        )

      }else{
        //both are empty nothing happens
        //console.log("guess stays the same: "+ guess)
      }
      
      
      
    });
    

  }
  private updateQuiz(team: App.Team){
    let index = this.quiz.teams.findIndex(element => team.id === element.id)
    this.quiz.teams[index] = team
    this.quizService.update(this.quiz).subscribe(data =>{
      console.log("Quiz updated")
  
    })

  }

  private logout(team: App.Team){
    this.localStorage.removeItem('teamId')
    this.isRegistered = false  
    this.router.navigate(['sign-in'],{relativeTo: this.route})
  }


  private isFilled(question : App.Question): boolean{
    let htmlId = "guess-"+ question.id
    if(this.form.get(htmlId).value) { return true }
    else{ return false }
  }
}
