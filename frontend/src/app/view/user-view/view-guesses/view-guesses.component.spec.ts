import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewGuessesComponent } from './view-guesses.component';

describe('ViewGuessesComponent', () => {
  let component: ViewGuessesComponent;
  let fixture: ComponentFixture<ViewGuessesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewGuessesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewGuessesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
