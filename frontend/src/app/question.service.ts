import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { App } from './model.interface';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuestionService { 
  private apiRoot = `${environment.backendUrl}/questions`; 
  private questions : App.Question[];

  constructor(private http: HttpClient, private router: Router) { }
  //create
  create(content: string, answer: string , round: App.Round ,shortlink?: string): Observable<App.Question> {
    let newQuestion = { 
      content: content,
      answer: answer,
      roundId: round.id,
      shortlink: shortlink || ''
    };
    let apiURL = `${this.apiRoot}`;
    return this.http.post<App.Question>(apiURL, newQuestion).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        console.log(response)
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )

  }
  //getall
  getAll(): Observable<App.Question[]>{
    let apiURL = `${this.apiRoot}`;
    return this.http.get<App.Question[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        this.questions = response
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  //get all of a round
  getByRound(round: App.Round): Observable<App.Question[]>{
    let apiURL = `${this.apiRoot}`;
    return this.http.get<App.Question[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let questionsInThatRound = response.filter(element => element.roundId === round.id)
        return questionsInThatRound;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  //get one by id
  getById(id: string): Observable<App.Question>{
    let apiURL = `${this.apiRoot}/${id}`;
    return this.http.get<App.Question>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
  //update
  update(newData: App.Question): Observable<App.Question>{
    let apiURL = `${this.apiRoot}/${newData.id}`;
    return this.http.put<App.Question>(apiURL, newData).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let index = this.questions.findIndex(Question => newData.id === Question.id);
        this.questions.splice(index, 1, newData)
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
  //delete
  delete(id: String): Observable<App.Question>{
    let apiURL = `${this.apiRoot}/${id}`;
    return this.http.delete<App.Question>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let index = this.questions.findIndex(Question => id === Question.id);
        this.questions.splice(index, 1)
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
  deleteByRound(roundId: string){
    this.questions.forEach((question, index) =>{
      if(question.roundId === roundId){
        let apiURL = `${this.apiRoot}/${question.id}`;
        return this.http.delete<App.Question>(apiURL).pipe(
          map(response => {
            if (!response) {
              return null;
            }
            this.questions.splice(index, 1)
            return response;
            
          }),
          catchError(res => this.handleError(res))
        ) 

      }

    })
    

  }

  private handleError(res): Observable<any>{
    //TODO navigate to Error page
    console.log(res.error)
    return of(res.error as any);
  }
}
