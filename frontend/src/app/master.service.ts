import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { App } from './model.interface';
import { QuizService } from './quiz.service';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  private apiRoot = `${environment.backendUrl}/master`; 
  private currentMaster: App.Master;
  

  constructor(private http: HttpClient, private router: Router) { }

  getCurrentMaster(): App.Master{
    return this.currentMaster;
  }
  
  validateLogin(master: App.Master): Observable<App.Master>{
    return this.http.post<App.Master>(`${environment.backendUrl}/login`, master).pipe(
      map(response => {

        if (!response) {
          
          return null;
        }
        this.currentMaster = response
        //console.log("Master validated")
       
        return response
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  //create
  create(name: string, password: string):Observable<App.Master>{
    let newMaster: App.Master = {
      name: name,
      password: password

    }

    return this.http.post<App.Master>(`${this.apiRoot}`, newMaster).pipe(
      map(response => {

        if (!response) {
          
          return null;
        }
        return response
        
      }),
      catchError(res => this.handleError(res))
    )
  }


  //get Master by Id
  getById(masterId: string):Observable<App.Master>{
    return this.http.get<App.Master>(`${this.apiRoot}/${masterId}`).pipe(
      map(response => {

        if (!response) {
          
          return null;
        }
        this.currentMaster = response
        
        //console.log("Master validated")
       
        return response
        
      }),
      catchError(res => this.handleError(res))
    )

  }

  update(newData: App.Master): Observable<App.Master>{
    return this.http.put<App.Master>(`${this.apiRoot}/${newData.id}`, newData).pipe(
      map(response => {

        if (!response) {
          
          return null;
        }
        this.currentMaster = response
        //console.log("Master validated")
       
        return response
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  delete(newData: App.Master): Observable<App.Master>{
    return this.http.delete<App.Master>(`${this.apiRoot}/${newData.id}`, newData.id).pipe(
      map(response => {

        if (!response) {
          
          return null;
        }
        this.currentMaster = null
       
        return response
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  private handleError(res): Observable<any>{
    if(res.status == 404 ){
      alert("Your MasterId doesnt exist on current Database. Renate probably used the wrong Url when hosting this app")
      localStorage.removeItem('masterId')
      this.router.navigate(['../login'])
    }
    else{
      //TODO navigate to Error page
      console.log(res.error)
      alert(res.error.message)
    }
    
    return of(res.error as any);
  }

}
