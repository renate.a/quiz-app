import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { MasterService } from './master.service';
import { App } from './model.interface';
import { QuestionService } from './question.service';

@Injectable({
  providedIn: 'root'
})
export class GuessService {
  private currentGuesses: App.Guess[]
  private apiRoot = `${environment.backendUrl}/guesses`; 
  constructor(private http: HttpClient, private masterService :MasterService) { }
/*
  getCurrent(): App.Guess[]{
    return this.currentGuesses
  }

  getByQuestion(question: App.Question): App.Guess[]{
    return this.currentGuesses.filter(element => element.question.id ===  question.id )
  }
*/
  //create
  create(content: string, question: App.Question,  team: App.Team, quiz: App.Quiz): Observable<App.Guess> {
    let newGuess = { 
      content: content,
      teamId: team.id,
      question: question,
      isCorrect: null
    };
    let apiURL = `${this.apiRoot}/${quiz.id}/`;
    return this.http.post<App.Guess>(apiURL, newGuess).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )

  }
  //getall
  getAll(): Observable<App.Guess[]>{
    let apiURL = `${this.apiRoot}`;
    return this.http.get<App.Guess[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  //getall
  getByTeam(team: App.Team, quiz: App.Quiz): Observable<App.Guess[]>{
    let apiURL = `${this.apiRoot}/${quiz.id}`;
    return this.http.get<App.Guess[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let teamGuesses = []
        teamGuesses = response.filter(element => element.teamId === team.id)

        return teamGuesses;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  getByQuiz(quiz: App.Quiz): Observable<App.Guess[]>{
    let apiURL = `${this.apiRoot}/${quiz.id}`;
    return this.http.get<App.Guess[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        //TODO if quiz is active, add to current
        //this.currentGuesses=
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }


  //get one by id
  getById(id: string, quiz: App.Quiz): Observable<App.Guess[]>{
    let apiURL = `${this.apiRoot}/${quiz.id}/id/${id}`;
    return this.http.get<App.Guess[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  //update
  update(newData: App.Guess, quiz: App.Quiz): Observable<App.Guess>{
    let apiURL = `${this.apiRoot}/${quiz.id}/id/${newData.id}`;
    return this.http.put<App.Guess>(apiURL, newData).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        //this.guesses.splice(this.guesses.findIndex(element => element.id === response.id), 1, response )
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
  //delete
  deleteById(id: string, quiz: App.Quiz){
    let apiURL = `${this.apiRoot}/${quiz.id}/id/${id}`;
    return this.http.put<App.Guess>(apiURL, id).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        //const index = this.guesses.findIndex(element => element.id === id)
        //this.guesses.splice(index,1)
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
    
    
  }
  deleteAll(quiz: App.Quiz){
    let apiURL = `${this.apiRoot}/${quiz.id}}`;
    return this.http.delete<App.Guess[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        console.log(response)
        //this.guesses = []
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
    
    
  }

  private handleError(res): Observable<any>{
    //TODO navigate to Error page
    console.log("error in guess.service")
    console.log(res.error)
    return of(res.error as any);
  }
}
