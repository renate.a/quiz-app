import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { GuessService } from './guess.service';
import { App } from './model.interface';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  private apiRoot = `${environment.backendUrl}/quizzes`;
  private currentQuiz : App.Quiz;
  //private activeQuestions: App.Quiz[];
  //private closedRounds: App.Round[];

  constructor(private http: HttpClient, private router: Router, private guessService: GuessService) { }

  public getCurrent(): App.Quiz{
    return this.currentQuiz
  }

  public setCurrent(quiz: App.Quiz){
    this.currentQuiz = quiz
  }
  //create
  create(title: string, masterId: string , image?: string, shortlink?: string): Observable<App.Quiz> {
    let newQuiz = { 
      title: title,
      masterId: masterId,
      image: image || "",
      shortlink: shortlink || '',
      rounds: [],
      teams: [],
      
    };
    let apiURL = `${this.apiRoot}`;
    return this.http.post<App.Quiz>(apiURL, newQuiz).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )

  }
  //getall
  getAll(): Observable<App.Quiz[]>{
    let apiURL = `${this.apiRoot}`;
    return this.http.get<App.Quiz[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }


  //get all Quizzes of one Quizmaster
  getByMaster(master: App.Master): Observable<App.Quiz[]>{
    let apiURL = `${environment.backendUrl}/master/${master.id}/quiz`;
    return this.http.get<App.Quiz[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let current: App.Quiz = response.find(element => {element.id === master.activeQuiz})
        if(current) {this.currentQuiz = current }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  setCurrentQuiz(id: string): Observable<App.Quiz>{
    let apiURL = `${this.apiRoot}/${id}`;
    return this.http.get<App.Quiz>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        this.currentQuiz = response;
        //this.guessService.getByQuiz(response).subscribe()
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  update(newData: App.Quiz, master?: App.Master): Observable<App.Quiz>{
    let apiURL = `${this.apiRoot}/${newData.id}`;
    return this.http.put<App.Quiz>(apiURL, newData).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        if(master){
            if(master.activeQuiz === response.id){
              this.currentQuiz = response;
            }
        }
        return response;
      }),
      catchError(res => this.handleError(res))
    )
  }

  getByLink(link: string, master?:App.Master): Observable<App.Quiz>{
    let apiURL = `${this.apiRoot}/view/${link}`;
    return this.http.get<App.Quiz>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        //console.log(response)
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }


  //get one by id
  getById(id: string, master?:App.Master): Observable<App.Quiz>{
    let apiURL = `${this.apiRoot}/${id}`;
    return this.http.get<App.Quiz>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }


//mini teamService
  createTeam(name: string, quiz:App.Quiz ): Observable<App.Quiz>{
    let newTeam = {
      name:name,
      score: 0,
      guesses: []
    }
    quiz.teams.push(newTeam)
    let apiURL = `${this.apiRoot}/${quiz.id}/newTeam`;
    return this.http.put<App.Team>(apiURL, quiz).pipe(
      map(response => {
        if (!response) {
          return null;
        }

        return response;
      }),
      catchError(res => this.handleError(res))
    )
  }

  updateTeam(newData: App.Team, quiz: App.Quiz): Observable<App.Quiz>{
    let index = quiz.teams.findIndex(element => element.id == newData.id)
    if(index >=0){
      quiz.teams.splice(index,1, newData)

      let apiURL = `${this.apiRoot}/${quiz.id}`;
      return this.http.put<App.Quiz>(apiURL, quiz).pipe(
        map(response => {
          if (!response) {
            return null;
          }
          return response;
        }),
        catchError(res => this.handleError(res))
      )

    }else{
      console.log("no team found to update")
      return null
    }
    

  }

  getAllTeams(quiz: App.Quiz):Observable<App.Team[]>{
    let apiURL = `${this.apiRoot}/${quiz.id}`;
    return this.http.get<App.Quiz>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        //this.quiz = response;
        return response.teams;
        
      }),
      catchError(res => this.handleError(res))
    )


  }


  deleteTeam(team: App.Team, quiz:App.Quiz ): Observable<App.Quiz>{
    let index = this.currentQuiz.teams.findIndex(element => element.id == team.id)
    this.currentQuiz.teams.splice(index,1)

    let apiURL = `${this.apiRoot}/${quiz.id}`;
    return this.update(quiz)

  }

  reset(quiz: App.Quiz, options?: string[]): Observable<App.Quiz>{
    let apiURL = `${this.apiRoot}/${quiz.id}`;
    if(options){
      if(options.includes("teams")){
        quiz.teams = []
        quiz.guesses= []
        
      }
      if(options.includes("guesses")){
        quiz.guesses= []
        //this.guessService.deleteAll(quiz).subscribe()
      }
      if(options.includes("rounds")){
        quiz.rounds.forEach(element =>{
          element.activatedQuestions = []
          element.isRunning = false;
          element.isClosed = false;
        })
      }

    }
    
    return this.http.put<App.Quiz>(apiURL, quiz).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        this.currentQuiz = quiz;
        return response;
      }),
      catchError(res => this.handleError(res))
    )
  }

  //delete
  delete(id: String){
    let apiURL = `${this.apiRoot}/${id}`;
    return this.http.delete<App.Quiz>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
  

  private handleError(res): Observable<any>{
    if(res.status == 404){
      alert("No Quiz with this LinkId was found. Ask your Quizmaster for the correct Link")
      this.router.navigate(['../../login'])
    }else{
      console.log(res.error)
    }
    //TODO navigate to Error page
    
    return of(res.error as any);
  }
  

  
}
