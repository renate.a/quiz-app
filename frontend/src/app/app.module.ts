import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app.routing';



import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';

import { ComponentsModule } from './components/components.module';
import { ExamplesModule } from './examples/examples.module';
import { ViewQuizComponent } from './view/master-view/quiz/view-quiz/view-quiz.component';
import { QuestionService } from './question.service';
import { HttpClientModule } from '@angular/common/http';
import { QuestionEditComponent } from './view/master-view/rounds/question-edit/question-edit.component';
import { QuizService } from './quiz.service';
import { DetailTeamComponent } from './view/master-view/teams/detail-team/detail-team.component';
import { ViewQuestionsComponent } from './view/master-view/rounds/view-questions/view-questions.component';
import { CheckRoundComponent } from './view/master-view/quiz/check-round/check-round.component';
import { RoundService} from './round.service';
import { ViewGuessesComponent } from './view/user-view/view-guesses/view-guesses.component';
import { SignInComponent } from './view/user-view/sign-in/sign-in.component';
import { GuessService } from './guess.service';
import { TeamNavigationComponent } from './view/master-view/teams/team-navigation/team-navigation.component';
import { TeamOverviewComponent } from './view/master-view/teams/team-overview/team-overview.component';
import { CreateQuizComponent } from './view/master-view/quiz/create-quiz/create-quiz.component';
import { QuizOverviewComponent } from './view/master-view/quiz/quiz-overview/quiz-overview.component';
import { LandingPageComponent } from './shared/landing-page/landing-page.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { MasterViewComponent } from './view/master-view/master-view.component';
import { ErrorpageComponent } from './shared/errorpage/errorpage.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    ViewQuizComponent,
    QuestionEditComponent,

    DetailTeamComponent,
    ViewQuestionsComponent,
    CheckRoundComponent,
    ViewGuessesComponent,
    SignInComponent,
    TeamNavigationComponent,
    TeamOverviewComponent,
    CreateQuizComponent,
    QuizOverviewComponent,
    LandingPageComponent,
    LoadingComponent,
    MasterViewComponent,
    ErrorpageComponent,

  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    RouterModule,
    ComponentsModule,
    ExamplesModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule
  ],
  providers: [QuizService, RoundService, GuessService],
  bootstrap: [AppComponent]
})
export class AppModule { }
