import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faBaby } from '@fortawesome/free-solid-svg-icons';
import { App } from 'app/model.interface';
import { MasterService } from 'app/master.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  private master: App.Master = {name: '', password:''}
  private masterId: string
  form = this.fb.group({
    name: ['', [Validators.required, Validators.min(1)]],
    password: ['', [Validators.required, Validators.min(1)]],
  })
  constructor(private fb: FormBuilder,private masterService: MasterService, private router: Router) { 
  }

  ngOnInit(): void {
    this.masterId = localStorage.getItem('masterId')
    if(this.masterId){
      this.router.navigate(['../master/quizzes'])
    }
  }

  validateLogin() {
    if(this.form.get('name').valid && this.form.get('password').valid) {
      this.master.name = this.form.get('name').value;
      this.master.password = this.form.get('password').value;
      this.masterService.validateLogin(this.master).subscribe(data => {
        if(data['status'] === 'success') {
          this.master =  data['data']
          console.log('login validated' +data['data'].name)
          localStorage.setItem("masterId", this.master.id);
          this.router.navigate(['master/quizzes']);
        } else {
          alert('Wrong username or password. Try again!');
        }
      }, error => {
        console.log('error is ', error);
      });
    } else {
      alert('enter user name and password');
    }
  }

  createMaster(){
    const name = this.form.get('name').value
    const pwd = this.form.get('password').value
    this.masterService.create(name, pwd).subscribe(data => {
      if(!data){
        console.log(data)
        this.master = data
        this.validateLogin()
       }
    },error => {
      console.log('error is: '+error);
    })
      
  }
    
}