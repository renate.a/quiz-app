import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthGuardService } from './auth-guard.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(private authService: AuthGuardService, private router: Router){}
  canActivate(): boolean {  
    if (!this.authService.getToken()) {  
        this.router.navigateByUrl("/login");  
    }  
    return this.authService.getToken();  
}  
  
}
