import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components/components.component';
import { ProfileComponent } from './examples/profile/profile.component';
import { SignupComponent } from './examples/signup/signup.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { ViewQuizComponent } from './view/master-view/quiz/view-quiz/view-quiz.component';
import { DetailTeamComponent } from './view/master-view/teams/detail-team/detail-team.component';
import { ViewQuestionsComponent } from './view/master-view/rounds/view-questions/view-questions.component';
import { CheckRoundComponent } from './view/master-view/quiz/check-round/check-round.component';
import { ViewGuessesComponent } from './view/user-view/view-guesses/view-guesses.component';
import { TeamNavigationComponent } from './view/master-view/teams/team-navigation/team-navigation.component';
import { TeamOverviewComponent } from './view/master-view/teams/team-overview/team-overview.component';
import { QuizOverviewComponent } from './view/master-view/quiz/quiz-overview/quiz-overview.component';
import { LandingPageComponent } from './shared/landing-page/landing-page.component';
import {AuthenticationGuard as AuthGuard } from './authentication.guard'
import { SignInComponent } from './view/user-view/sign-in/sign-in.component';
import { MasterViewComponent } from './view/master-view/master-view.component';
import { ErrorpageComponent } from './shared/errorpage/errorpage.component';

const routes: Routes =[
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login',            component: LandingPageComponent},

    { path: 'quiz/:linkId',             component: ViewGuessesComponent },
    { path: 'quiz/:linkId/sign-in',     component: SignInComponent },
    { path: "quiz/**", redirectTo: 'login' },
    { path: 'master', component: MasterViewComponent, canActivate: [AuthGuard],
      children: [
        { path: '', redirectTo: 'quizzes', pathMatch: 'full'},
        { path: 'active',             component: ViewQuizComponent , canActivate: [AuthGuard]},
        { path: 'quizzes',         component: QuizOverviewComponent },
        
        { path: 'questions',        component: ViewQuestionsComponent },
        {
          path: 'teams',
          component: TeamNavigationComponent, // this is the component with the <router-outlet> in the template
          children: [
            { path: '', redirectTo: 'overview', pathMatch: 'full'},
            {
              path: 'overview', // child route path
              component: TeamOverviewComponent, // child route component that the router renders
            },
            {
              path: ':id',
              component: DetailTeamComponent, // another child route component that the router renders
            },
          ],
        },
        { path: 'check-answers',  component: CheckRoundComponent },

      ]},
    { path: 'error',            component: ErrorpageComponent},
      
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
