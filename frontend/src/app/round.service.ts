import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { App } from './model.interface';
import { QuestionService } from './question.service';

@Injectable({
  providedIn: 'root'
})
export class RoundService {
  private apiRoot = `${environment.backendUrl}/rounds`; 
  private currentRounds: App.Round[];

  constructor(private http: HttpClient, private router: Router, private questionService: QuestionService) { }
  
  public getCurrent(): App.Round[]{
    return this.currentRounds
  }
  //create
  create(newRound: App.Round): Observable<App.Round> {
    let apiURL = `${this.apiRoot}`;
    return this.http.post<App.Round>(apiURL, newRound).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        this.currentRounds.push(response)
        console.log("successfully created round")
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )

  }
  //getall
  getAll(): Observable<App.Round[]>{
    let apiURL = `${this.apiRoot}`;
    return this.http.get<App.Round[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

  //get all  Rounds of one Quizmaster
  getByMaster(master: App.Master): Observable<App.Round[]>{
    let apiURL = `${environment.backendUrl}/master/${master.id}/rounds`;
    return this.http.get<App.Round[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        this.currentRounds = response
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }

/*

  getAllQuestions(): Observable<App.Question[]>{
    let apiURL = `${this.apiRoot}`;
    return this.http.get<App.Round[]>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let allQuestions: App.Question[] =[]
        response.forEach(element => {
          allQuestions.push(...element.questions)
        })
        return allQuestions;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
*/
  
  //get one by id
  getById(id: string): Observable<App.Round>{
    let apiURL = `${this.apiRoot}/${id}`;
    return this.http.get<App.Round>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
  //update
  update(newData: App.Round): Observable<App.Round>{
    let apiURL = `${this.apiRoot}/${newData.id}`;
    return this.http.put<App.Round>(apiURL, newData).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let index = this.currentRounds.findIndex(round => response.id === round.id);
        if(index >= 0) this.currentRounds.splice(index, 1, response)
        
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
  }
  //delete
  delete(id: string){
    let apiURL = `${this.apiRoot}/${id}`;
    return this.http.delete<App.Round>(apiURL).pipe(
      map(response => {
        if (!response) {
          return null;
        }
        let index = this.currentRounds.findIndex(round => response.id === round.id);
        if(index >= 0) {this.currentRounds.splice(index, 1)}
        return response;
        
      }),
      catchError(res => this.handleError(res))
    )
    
    
  }

  private handleError(res): Observable<any>{
    //TODO navigate to Error page
    console.log(res.error)
    return of(res.error as any);
  }
}
