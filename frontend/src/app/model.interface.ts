export namespace App{
    export interface Quiz {
        id?: string;
        shortlink?: string;
        title: string;
        masterId: string;
        image?: string;
        rounds: App.QuizRound[];
        teams?: App.Team[];
        guesses?: App.Guess[]
        isActive?: boolean
    }
    


    export interface QuizRound{
        round: App.Round, 
        activatedQuestions?: App.Question[], 
        isRunning?: boolean, 
        isClosed?: boolean
    }

    export interface Round{
        id?: string;
        title: string;
        questions: App.Question[];
        masterId: string
    }

    export interface Question{
        id?: string;
        roundId: string;
        nr?: number;
        content: string;
        answer: string;
    }

    export interface Team{
        id?: string;
        name: string;
        score: number;
        guesses?: App.Guess[];
    }

    export interface Master{
        id?
        name: string,
        password: string
        quizzes?: App.Quiz[]
        rounds?: App.Round[]
        activeQuiz?: string
    }

    export interface Guess{
        id?: string,
        question: App.Question,
        content: string;
        teamId: string;
        isCorrect?: boolean;
    }
}

