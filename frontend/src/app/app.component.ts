import { Component, OnInit, Inject, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';
import { DOCUMENT } from '@angular/common';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { QuestionService } from './question.service';
import { QuizService } from './quiz.service';
import { RoundService } from './round.service';
import { GuessService } from './guess.service';
import { MasterService } from './master.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    private _router: Subscription;
    private isReady: number = 0;
    masterId: string;
    private hasLoaded: number = 0
    private hasMaster: boolean
    
    
    //@ViewChild(NavbarComponent) navbar: NavbarComponent;

    constructor(private quizService: QuizService, private masterService: MasterService, private roundService: RoundService, private guessService: GuessService, private renderer : Renderer2, private router: Router, @Inject(DOCUMENT,) private document: any, private element : ElementRef, public location: Location) {}
    ngOnInit() {
        this.renderer.listen('window', 'scroll', (event) => {
            const number = window.scrollY;
            if (number > 150 || window.pageYOffset > 150) {
                // add logic
                //navbar.classList.remove('navbar-transparent');
            } else {
                // remove logic
                //navbar.classList.add('navbar-transparent');
            }
        });
        var ua = window.navigator.userAgent;
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            var version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        if (version) {
            var body = document.getElementsByTagName('body')[0];
            body.classList.add('ie-background');

        }
        
        
        this.masterId = localStorage.getItem('masterId') || ''
        if(!this.masterId.length){
            this.router.navigate(["/login"])
        }

    }
/*
    loadData(masterId: string){
        this.masterService.getById(masterId).subscribe(
            master => {
                if(master){
                    console.log("master is: "+ master.name)
                    this.roundService.getByMaster(master).subscribe(
                        rounds =>{
                            console.log("master has rounds: "+ rounds.length);
                            this.hasLoaded ++
                        });
                    this.quizService.getByMaster(master).subscribe(
                        
                        quizzes => {
                            console.log("master has quizzes: "+ quizzes.length)
                            let active = quizzes.find(element => element.id === master.activeQuiz)
                            if(active){
                                this.quizService.setCurrent(active)
                                this.guessService.guessesOfQuiz(active).subscribe(data =>{
                                    console.log("current is: "+ active.title)
                                })
                            }
                          this.hasLoaded ++    
                          this.hasMaster = true
                      })

                }else{
                    this.router.navigate(["/login"])
                    this.hasLoaded = 0
                }
                
            },() => {},() =>{
                //complete
                
            })
    }
*/ 

    
    removeFooter() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice( 1 );
        if(titlee === 'signup' || titlee === 'nucleoicons'){
            return false;
        }
        else {
            return true;
        }
    }
}
