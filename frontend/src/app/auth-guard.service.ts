import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationGuard } from './authentication.guard';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router) { }

  getToken(){
    return !!localStorage.getItem("masterId");  
  }

}
